package models

import (
	"bytes"
	"encoding/gob"

	"github.com/boltdb/bolt"
)

// DB represents an open Key-Value database. Internally, it uses Bolt.
type DB struct {
	db *bolt.DB
}

// Open creates a new database, by opening the specified file. It also goes
// through the required initialization operations.
func Open(fileName string) (*DB, error) {
	db, err := bolt.Open(fileName, 0600, nil)
	if err != nil {
		return nil, err
	}
	ret := &DB{
		db: db,
	}
	err = ret.runMigrations()
	return ret, err
}

var (
	byteStats   = []byte("stats")
	byteVersion = []byte("version")
)

func (d *DB) runMigrations() error {
	err := d.db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists(byteStats)
		if err != nil {
			return err
		}

		// not an ID, but we want to use binary (un)marshaling features
		i := new(ID)
		i.UnmarshalBinary(b.Get(byteVersion))
		if int(*i) >= len(migrations) {
			return nil
		}

		pending := migrations[*i:]
		for idx, mig := range pending {
			err = mig(tx)
			if err != nil {
				return err
			}
			data, _ := ID(idx + 1).MarshalBinary()
			err = b.Put([]byte(byteVersion), data)
			if err != nil {
				return err
			}
		}
		return nil
	})
	return err
}

var migrations = [...]func(tx *bolt.Tx) error{
	func(tx *bolt.Tx) error { return createBuckets(tx, byteUsers, byteUsersByUsername) },
	func(tx *bolt.Tx) error { return createBuckets(tx, byteAttempts) },
}

func createBuckets(tx *bolt.Tx, bucks ...[]byte) error {
	var err error
	for _, b := range bucks {
		_, err = tx.CreateBucket([]byte(b))
		if err != nil {
			return err
		}
	}
	return nil
}

// Close closes the underlying database.
func (d *DB) Close() error {
	return d.db.Close()
}

func (d *DB) readModel(bucket, key []byte, into interface{}) error {
	return d.db.View(func(tx *bolt.Tx) error {
		data := tx.Bucket(bucket).Get(key)
		if len(data) == 0 {
			return nil
		}
		return gobUnmarshal(data, into)
	})
}

func gobMarshal(v interface{}) ([]byte, error) {
	buf := new(bytes.Buffer)
	err := gob.NewEncoder(buf).Encode(v)
	return buf.Bytes(), err
}

func gobUnmarshal(b []byte, v interface{}) error {
	return gob.NewDecoder(bytes.NewReader(b)).Decode(v)
}
