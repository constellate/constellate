package models

import (
	"testing"
	"time"
)

func TestAttempt(t *testing.T) {
	defer dbClose(t)
	db := testDB(t)

	t.Run("Simple", func(t *testing.T) {
		var k = []byte("test1")
		res, err := db.Attempt(k, 0, 1)
		equal(t, err, nil)
		equal(t, res, uint64(1))

		// delta of 3
		res, err = db.Attempt(k, 0, 3)
		equal(t, err, nil)
		equal(t, res, uint64(4))

		// no delta; read-only
		res, err = db.Attempt(k, 0, 0)
		equal(t, err, nil)
		equal(t, res, uint64(4))
	})
	t.Run("WithTimeFrame", func(t *testing.T) {
		var k = []byte("test2")
		res, err := db.Attempt(k, time.Second, 1)
		equal(t, err, nil)
		equal(t, res, uint64(1))

		res, err = db.Attempt(k, time.Second, 1)
		equal(t, err, nil)
		equal(t, res, uint64(2))

		time.Sleep(time.Second)

		res, err = db.Attempt(k, time.Second, 1)
		equal(t, err, nil)
		equal(t, res, uint64(1))
	})
}
