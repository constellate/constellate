package models

import (
	"time"

	"github.com/boltdb/bolt"
)

var byteAttempts = []byte("attempts")

// Attempt atomically increments the number of attempts done for doing id in the
// specified timeFrame. id is generally a combination of the resource being
// limited and an identifier of an user - typically an IP address or an ID. For
// instance, "signup:127.0.0.1" might be a good id.
//
// If timeFrame is specified, then the attempt count "rolls over" to 0 once
// a new timeFrame arrives. i.e.: with time.Minute, calling Attempt at 19:03:59
// returns 1312 (for example), 19:04:00 returns 0.
//
// Delta is the amount of attempts that should be added. If <= 0, Attempt will
// be read-only.
func (d *DB) Attempt(id []byte, timeFrame time.Duration, delta int) (uint64, error) {
	// We use ID because it is really easy to binary encode/decode.
	var out, currentFrame ID
	if timeFrame != 0 {
		currentFrame = ID(time.Now().Truncate(timeFrame).Unix())
	}

	fn := func(tx *bolt.Tx) error {
		buck := tx.Bucket(byteAttempts)

		// retrieve the value of the id - if it was previously set, we check
		// that the frame is the same as the one we currently hold, and if so
		// we set our resulting value to be the previous before increasing it.
		b := buck.Get(id)
		if len(b) == 16 {
			dbFrame := new(ID)
			dbFrame.UnmarshalBinary(b[:8])
			if *dbFrame == currentFrame {
				(&out).UnmarshalBinary(b[8:])
			}
		}

		// read only op: return before modifying
		if delta <= 0 {
			return nil
		}

		out += ID(delta)

		// encode final result
		result := append(currentFrame.Binary(), out.Binary()...)
		return buck.Put(id, result)
	}

	var err error
	if delta <= 0 {
		err = d.db.View(fn)
	} else {
		err = d.db.Batch(fn)
	}
	return uint64(out), err
}
