package models

import (
	"errors"
	"fmt"
	"os"
	"testing"

	"github.com/boltdb/bolt"
)

func testDB(t *testing.T) *DB {
	db, err := Open(t.Name() + ".test.db")
	equal(t, err, nil)
	return db
}

func dbClose(t *testing.T) {
	os.Remove(t.Name() + ".test.db")
}

func TestOpen(t *testing.T) {
	defer dbClose(t)
	db := testDB(t)
	err := db.db.View(func(tx *bolt.Tx) error {
		buck := tx.Bucket(byteStats)
		if buck == nil {
			return errors.New("buck is nil")
		}
		id := new(ID)
		version := buck.Get(byteVersion)
		id.UnmarshalBinary(version)
		// after a successful Open, we should always be at the latest version
		if int(*id) != len(migrations) {
			return fmt.Errorf("not at latest version; got %d want %d", *id, len(migrations))
		}
		return nil
	})
	if err != nil {
		t.Error(err)
	}
	err = db.Close()
	if err != nil {
		t.Error(err)
	}
}
