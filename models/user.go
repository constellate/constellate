package models

import (
	"encoding/gob"
	"errors"

	"github.com/boltdb/bolt"
)

// User represents a single user
type User struct {
	ID       ID
	Username string
	Name     string
	Email    string
	Password []byte
}

func init() { gob.Register(User{}) }

var (
	byteUsers           = []byte("users")
	byteUsersByUsername = []byte("usersByUsername")
)

// User retrieves an user from the database, knowing the ID.
func (d *DB) User(id ID) (*User, error) {
	u := new(User)
	return u, d.readModel(byteUsers, id.Binary(), u)
}

// UserByUsername retrieves an user from the database, knowing the username.
func (d *DB) UserByUsername(name string) (*User, error) {
	u := new(User)
	return u, d.db.View(func(tx *bolt.Tx) error {
		id := tx.Bucket(byteUsersByUsername).Get([]byte(name))
		if len(id) == 0 {
			return nil
		}
		data := tx.Bucket(byteUsers).Get(id)
		if len(data) == 0 {
			return nil
		}
		return gobUnmarshal(data, u)
	})
}

// Errors that may happen when calling SetUser and that are to be expected.
var (
	ErrUsernameExists = errors.New("models: username exists, aborting user creation")
)

// SetUser updates or creates an user in the database.
func (d *DB) SetUser(u *User) error {
	var isNew bool
	if u.ID == 0 {
		u.ID = GenerateID()
		isNew = true
	}
	return d.db.Batch(func(tx *bolt.Tx) error {
		var ubuBucket *bolt.Bucket
		if isNew {
			ubuBucket = tx.Bucket(byteUsersByUsername)
			if len(ubuBucket.Get([]byte(u.Username))) > 0 {
				return ErrUsernameExists
			}
		}
		data, err := gobMarshal(u)
		if err != nil {
			return err
		}
		err = tx.Bucket(byteUsers).Put(u.ID.Binary(), data)
		if err != nil {
			return err
		}
		if isNew {
			err = ubuBucket.Put([]byte(u.Username), u.ID.Binary())
		}
		return err
	})
}
