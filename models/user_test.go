package models

import (
	"testing"
)

func TestUser(t *testing.T) {
	defer dbClose(t)
	db := testDB(t)

	var johnID ID

	t.Run("SetUser", func(t *testing.T) {
		usr := &User{
			Username: "john",
			Name:     "John Micheals",
			Email:    "john@example.com",
		}
		err := db.SetUser(usr)
		equal(t, err, nil)
		johnID = usr.ID
	})
	t.Run("User", func(t *testing.T) {
		user, err := db.User(johnID)
		equal(t, err, nil)
		equal(t, user.Name, "John Micheals")
		equal(t, user.Email, "john@example.com")
	})
	t.Run("UserByUsername", func(t *testing.T) {
		user, err := db.UserByUsername("john")
		equal(t, err, nil)
		equal(t, user.ID, johnID)
	})
	t.Run("UpdateUser", func(t *testing.T) {
		err := db.SetUser(&User{
			ID:       johnID,
			Username: "john",
			Name:     "John Micheals",
			Email:    "my.new@email.com",
		})
		equal(t, err, nil)
	})
	t.Run("UserWithUpdatedInfo", func(t *testing.T) {
		user, err := db.User(johnID)
		equal(t, err, nil)
		equal(t, user.Name, "John Micheals")
		equal(t, user.Email, "my.new@email.com")
	})
}
