package main

import (
	"fmt"
	"os"

	"github.com/alecthomas/kingpin"
	"github.com/erikdubbelboer/fasthttp"

	"gitlab.com/constellate/constellate/http"
	"gitlab.com/constellate/constellate/models"

	_ "gitlab.com/constellate/constellate/http/frontend"
)

var (
	port = kingpin.Flag("port", "Port on which to take incoming connections.").Default(":3000").Envar("PORT").String()
)

func main() {
	kingpin.Parse()

	db, err := models.Open("constellate.db")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	fmt.Println("Listening on", *port)

	handler := http.Handler(http.Options{
		DB: db,
	})
	err = fasthttp.ListenAndServe(*port, handler)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
