package http

import (
	"errors"
	"reflect"
)

// Validator is a generic interface for any struct that can self-check that its
// fields are valid.
type Validator interface {
	Validate() error
}

var (
	errVIsNil             = errors.New("http: v is nil")
	errNotPointerToStruct = errors.New("http: v is not a pointer to a struct")
)

// UnmarshalForm allows you to decode a form into v. It assumes v is a struct,
// and it will determine the underlying name of each field by getting the
// corresponding name via the struct tag `form` if available, or if not it will
// simply get the field's name. After decoding, it will run the validator to
// determine if the input is valid.
func (c *Context) UnmarshalForm(v Validator) error {
	if v == nil {
		return errVIsNil
	}

	// check that v is a pointer to struct, and set x to be the pointer's
	// underlying element.
	x := reflect.ValueOf(v)
	if x.Kind() != reflect.Ptr {
		return errNotPointerToStruct
	}
	x = x.Elem()
	if x.Kind() != reflect.Struct {
		return errNotPointerToStruct
	}

	xType := x.Type()
	numField := xType.NumField()
	for i := 0; i < numField; i++ {
		// get fType and make sure it is valid.
		fType := xType.Field(i)
		if fType.Anonymous {
			continue
		}

		// determine name: if we have struct tag use that, otherwise just use
		// name
		name := fType.Name
		if tagName := fType.Tag.Get("form"); tagName != "" {
			name = tagName
		}

		switch fType.Type.Kind() {
		case reflect.String:
			x.Field(i).SetString(b2s(c.Form(name)))
		case reflect.Slice:
			// byte slice
			if fType.Type.Elem().Kind() == reflect.Uint8 {
				x.Field(i).SetBytes(c.Form(name))
			}
		}
	}

	return v.Validate()
}
