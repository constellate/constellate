// Code generated by hero.
// source: signup.html
// DO NOT EDIT!
package templates

import (
	"github.com/thehowl/hero"
	"github.com/valyala/bytebufferpool"
)

func SignUp(buf *bytebufferpool.ByteBuffer) {
	buf.WriteString(`
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>`)
	if title != "" {
		hero.EscapeHTML(title, buf)
		buf.WriteString(` | `)
	}
	buf.WriteString(` Constellate</title>
        <link href="/static/css/style.css" rel="stylesheet">
    </head>

    <body>
        <div id="outer-container">
            <div id="inner-container">
                `)
	buf.WriteString(`
	<form id="signup-form" class="form" method="POST" action="/signup">
		<label>
			<span>Username</span>
			<input type="text" name="Username" maxlength="20" required>
		</label>
		<label>
			<span>Email</span>
			<input type="email" name="Email" required>
		</label>
		<label>
			<span>Password</span>
			<input type="password" name="Password" minlength="8" required>
		</label>
		<button type="submit">Sign up</button>
	</form>
`)

	buf.WriteString(`
            </div>
        </div>
    </body>
</html>
`)

}
