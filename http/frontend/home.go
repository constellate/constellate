package frontend

import (
	"gitlab.com/constellate/constellate/http"
)

// Home handles requests to the homepage.
func Home(c *http.Context) {
	c.WriteString("Hello world!\n")
}

func init() {
	http.GET("/", Home)
}
