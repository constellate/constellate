package frontend

import (
	"fmt"
	"regexp"
	"strings"
	"time"

	"gitlab.com/constellate/constellate/http"
	"gitlab.com/constellate/constellate/http/frontend/templates"
	"gitlab.com/constellate/constellate/models"

	"github.com/cespare/xxhash"
	"github.com/valyala/bytebufferpool"
	"golang.org/x/crypto/bcrypt"
)

type problemList []string

func (p problemList) add(condition bool, prob string, i int) int {
	if condition {
		// create new problem at position i and increment it
		p[i] = prob
		return i + 1
	}
	return i
}

func (p problemList) Error() string {
	if len(p) == 0 {
		return ""
	}
	return fmt.Sprintf("%d errors, of which: %s", len(p), p[0])
}

var usernameRegex = regexp.MustCompile("^[0-9a-zA-Z_]*$")

type signUpForm struct {
	Username string
	Email    string
	Password []byte
}

func passwordCommon(s string) bool {
	for _, p := range topPasswords {
		if s == p {
			return true
		}
	}
	return false
}

func (s *signUpForm) Validate() error {
	l := make(problemList, 7)
	i := l.add(s.Username == "", "Username is empty", 0)
	i = l.add(s.Email == "", "Email is empty", i)
	i = l.add(len(s.Password) == 0, "Password is empty", i)
	i = l.add(len(s.Username) > 20, "Username is too long (max 20 characters)", i)
	i = l.add(!strings.Contains(s.Email, "@"), "Email is invalid", i)
	i = l.add(len(s.Password) < 8, "Password is too short (at least 8 characters)", i)
	i = l.add(passwordCommon(string(s.Password)),
		"Password is on the top 10,000 list of most common passwords, we can't allow you to use that", i)
	if i == 0 {
		return nil
	}
	return l[:i]
}

// SignUp handles signup requests.
func SignUp(ctx *http.Context) {
	f := new(signUpForm)
	err := ctx.UnmarshalForm(f)
	switch err := err.(type) {
	case nil: // carry on
	case problemList:
		for _, prob := range err {
			ctx.WriteString("* " + prob + "\n")
		}
		return
	default:
		ctx.Error(err)
		return
	}

	// generate attempt ID, and check that we haven't already done 5 signups
	// with this IP in the last 24 hours.
	ipHash := models.ID(xxhash.Sum64([]byte(ctx.IP().String()))).Binary()
	attemptID := append([]byte("signup:"), ipHash...)
	signups, err := ctx.DB.Attempt(attemptID, time.Hour*24, 0)
	if err != nil {
		ctx.Error(err)
		return
	}
	if signups > 5 {
		ctx.SetCode(429)
		ctx.WriteString("Too many sign ups, try again later")
		return
	}

	pass, err := bcrypt.GenerateFromPassword([]byte(f.Password), bcrypt.DefaultCost)
	if err != nil {
		ctx.Error(err)
		return
	}

	usr := &models.User{
		Username: f.Username,
		Name:     f.Username,
		Email:    f.Email,
		Password: pass,
	}
	err = ctx.DB.SetUser(usr)
	switch err {
	case nil: // carry on
	case models.ErrUsernameExists:
		ctx.SetCode(409)
		ctx.WriteString("Username exists")
		return
	default:
		ctx.Error(err)
		return
	}

	// increase signups by 1
	_, err = ctx.DB.Attempt(attemptID, time.Hour*24, 1)
	if err != nil {
		ctx.Error(err)
		return
	}

	ctx.WriteString(fmt.Sprintf("User registered: %s\nID: %s\nYou may now log in\n", usr.Username, usr.ID))
}

// type aliasing is cool.
type bbuffer = *bytebufferpool.ByteBuffer

func html(ctx *http.Context, f func(buf bbuffer)) {
	ctx.SetHeader("Content-Type", "text/html; charset=utf-8")
	buf := bytebufferpool.Get()
	f(buf)
	ctx.SetBody(buf.B)
	bytebufferpool.Put(buf)
}

func htmlf(f func(buf bbuffer)) func(ctx *http.Context) {
	return func(ctx *http.Context) {
		html(ctx, f)
	}
}

func init() {
	http.GET("/signup", htmlf(func(b bbuffer) { templates.SignUp(b) }))
	http.POST("/signup", SignUp)
}
