package astreams

import (
	"time"

	"gitlab.com/constellate/ld"
)

//ld:prefix https://www.w3.org/ns/activitystreams#

// Note represents a short written work typically less than a single paragraph
// in length.
type Note struct {
	ID           string
	Content      string
	AttributedTo string
	To           []string
}

// Image represents an image document of any kind.
type Image struct {
	Name      string
	URL       string
	Summary   string
	Width     int
	Height    int
	MediaType string
}

// ===========
// Actor types

// Actor types are Object types that are capable of performing activities.
type Actor struct {
	ID                string
	Name              string
	PreferredUsername string
	Icon              *Image
	Image             *Image
	Published         time.Time
	StartTime         time.Time
	Summary           string
	Updated           time.Time
	URL               string
	Inbox             string
	Outbox            string
	Followers         string
	Following         string
	Liked             string
}

// Application describes a software application.
type Application struct {
	Actor
}

// Group represents a formal or informal collective of Actors.
type Group struct {
	Actor
}

// Person represents an individual person
type Person struct {
	Actor
}

// Service represents a service of any kind.
type Service struct {
	Actor
}

// ==============
// Activity Types

// An Activity is a subtype of Object that describes some form of action that
// may happen, is currently happening, or has already happened. The Activity
// type itself serves as an abstract base type for all types of activities. It
// is important to note that the Activity type itself does not carry any
// specific semantics about the kind of action being taken.
type Activity struct {
	ID      string
	Summary string
	Actor   string
	Object  *ld.Object
}

// Create indicates that the actor has created the object.
type Create struct {
	Activity
}
