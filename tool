#!/bin/bash

# provides documentation for the sub-commands.
DOCUMENTATION=(
	'help [COMMAND]'
	"returns help for the specified command, or for all commands if none is specified.
		$ ./tool help
		$ ./tool help sink"

	'npm-install'
	"run 'yarn' if available, 'npm install' otherwise, or if none are present exit.
		$ ./tool npm-install"

	'build-less'
	"compiles the Less code into CSS
		$ ./tool build-less"

	'build-templates'
	"build template files. Requires hero (go get github.com/thehowl/hero)
		$ ./tool build-templates"
)

runScript() {
	case $1 in
		npm-install)
			if command -v yarn; then
				yarn
			elif command -v npm; then
				npm install
			else
				echo "You do not have a node package manager installed - please get yarn or npm"
				exit 1
			fi
			;;

		# ----------------------------------------------------------------------
		build-less)
			if [ ! -x node_modules/.bin/lessc ]; then
				runScript npm-install
			fi
			node_modules/.bin/lessc --no-ie-compat --clean-css static/less/style.less static/css/style.css
			;;

		# ----------------------------------------------------------------------
		build-templates)
			hero -source http/frontend/templates/ -pkgname templates
			;;

		# ----------------------------------------------------------------------
		*)
			BOLD='\033[1m'
			NC='\033[0m'
			# command called with no args
			if [ "$1" == 'help' ]; then
				printf "${BOLD}tool${NC} "
				echo 'answers the question "what if you need to use make?" but'
				echo 'you obsess over simplicity and prefer to make a bash script.'
				echo 'It collects a bunch of utilities that you may (or may not)'
				echo 'find useful in the development workflow of Constellate.'
				echo
			else
				printf "Usage: ${BOLD}./tool <SUBCOMMAND> [<ARGUMENT>...]${NC}\n"
			fi
			if [ -z "$2" ]; then
				echo 'Subcommands:'
			fi

			idx=0
			found=false
			while [ "${DOCUMENTATION[idx]}" ]; do
				stringarr=(${DOCUMENTATION[idx]})
				# if $2 is set and this is the wrong command, then skip it
				if [ -n "$2" -a "${stringarr[0]}" != "$2" ]; then
					((idx+=2))
					continue
				fi

				found=true

				echo
				printf "\t${BOLD}${DOCUMENTATION[idx]}${NC}\n"
				printf "\t\t${DOCUMENTATION[idx+1]}\n"

				((idx+=2))
			done
			if [ "$found" = false ]; then
				echo
				echo "The specified subcommand $2 could not be found."
			fi
			;;
	esac
}

runScript $@
