package ld

import (
	"encoding/json"
	"strings"
	"testing"
)

func BenchmarkValueSlice_encodeBinary(b *testing.B) {
	parseValues := func(s string) (ValueSlice, error) {
		return ParseValues(json.NewDecoder(strings.NewReader(s)))
	}
	b.Run("SimpleObject", func(b *testing.B) {
		vals, err := parseValues(simpleObject)
		equal(b, err, nil)
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			vals.encodeBinary()
		}
	})
	b.Run("MoonFlight", func(b *testing.B) {
		vals, err := parseValues(moonFlight)
		equal(b, err, nil)
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			vals.encodeBinary()
		}
	})
	b.Run("MastodonProfile", func(b *testing.B) {
		vals, err := parseValues(mastodonProfile)
		equal(b, err, nil)
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			vals.encodeBinary()
		}
	})
}
