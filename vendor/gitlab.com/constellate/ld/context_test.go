package ld

import (
	"encoding/json"
	"strings"
	"testing"
)

const mastodonContext = `
[
  "https://www.w3.org/ns/activitystreams",
  "https://w3id.org/security/v1",
  {
    "manuallyApprovesFollowers": "as:manuallyApprovesFollowers",
    "sensitive": "as:sensitive",
    "Hashtag": "as:Hashtag",
    "ostatus": "http://ostatus.org#",
    "atomUri": "ostatus:atomUri",
    "inReplyToAtomUri": "ostatus:inReplyToAtomUri",
    "conversation": "ostatus:conversation",
    "toot": "http://joinmastodon.org/ns#",
    "Emoji": "toot:Emoji"
  }
]`

func TestSimpleContext(t *testing.T) {
	t.Parallel()
	tt := []struct {
		data string
	}{
		{
			`
{
	"name": "http://schema.org/name",
	"image": {
		"@id": "http://schema.org/image",
		"@type": "@id"
	},
	"homepage": {
		"@id": "http://schema.org/url",
		"@type": "@id"
	}
}`,
		},
		{
			`
  {
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "name": "http://xmlns.com/foaf/0.1/name",
    "age":
    {
      "@id": "http://xmlns.com/foaf/0.1/age",
      "@type": "xsd:integer"
    },
    "homepage":
    {
      "@id": "http://xmlns.com/foaf/0.1/homepage",
      "@type": "@id"
    }
  }`,
		},
		{
			mastodonContext,
		},
	}
	proc := &processor{}
	proc.Loader = commonLoader
	for _, test := range tt {
		r := strings.NewReader(test.data)
		dec := json.NewDecoder(r)
		dec.UseNumber()

		vals, err := ParseValues(dec)
		if err != nil {
			t.Error(err)
			continue
		}

		proc.ActiveContext = &context{}
		proc.LocalContexts = vals

		err = proc.processContext(nil)
		if err != nil {
			t.Error(err)
			continue
		}
	}
}

func BenchmarkProcessSmallContext(b *testing.B) {
	proc := &processor{}
	r := strings.NewReader(`
{
	"name": "http://schema.org/name",
	"image": {
		"@id": "http://schema.org/image",
		"@type": "@id"
	},
	"homepage": {
		"@id": "http://schema.org/url",
		"@type": "@id"
	}
}`)
	dec := json.NewDecoder(r)
	dec.UseNumber()
	vals, err := ParseValues(dec)
	if err != nil {
		b.Error(err)
	}
	proc.LocalContexts = vals
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		proc.processContext(nil)
	}
}

func BenchmarkProcessMastodonContext(b *testing.B) {
	proc := &processor{}
	proc.Loader = commonLoader
	r := strings.NewReader(mastodonContext)
	dec := json.NewDecoder(r)
	dec.UseNumber()
	vals, err := ParseValues(dec)
	if err != nil {
		b.Error(err)
	}
	proc.LocalContexts = vals

	// we need to do a warmup in order for the context requests to be cached.
	proc.processContext(nil)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		proc.processContext(nil)
	}
}
