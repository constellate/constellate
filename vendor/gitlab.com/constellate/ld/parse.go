package ld

import (
	"encoding/json"
	"errors"
)

// kind specifies, in a value, the type of the value. This is JSON's primitive
// values: null, bools, objects, numbers and strings.
type kind byte

const (
	kindNull kind = iota
	kindTrue
	kindFalse

	kindObject

	kindNumber
	kindString
)

var kindToString = [...]string{
	"null",
	"true",
	"false",
	"(object)",
	"(number)",
	"(string)",
}

func (k kind) String() string {
	return kindToString[int(k)]
}

// ValueSlice is a slice of value. value is mostly an internal struct of the ld
// package, and it is used for encoding and decoding JSON objects. You should
// only ever need to use this when making a custom DocumentLoader.
type ValueSlice []value

func (vs ValueSlice) String() string {
	s := "["
	for i, v := range vs {
		s += v.Str()
		if i != len(vs)-1 {
			s += ", "
		}
	}
	return s + "]"
}

type value struct {
	Kind   kind
	Object object
	String string
}

// Can't name String because that's already the name of the field.
func (v value) Str() string {
	if v.Kind == kindObject {
		return v.Object.String()
	}
	s := v.Kind.String()
	if v.Kind >= kindNumber {
		s += " " + v.String
	}
	return s
}

func (v value) Empty() bool {
	switch v.Kind {
	case kindNull, kindFalse:
		return true
	case kindString:
		return v.String == ""
	case kindNumber:
		return v.String == "0" || v.String == ""
	case kindObject:
		return len(v.Object.KeyMap) == 0
	}
	return false
}

type objectMapping struct {
	Key      string
	Values   ValueSlice
	IsSingle bool
}

func (m objectMapping) String() string {
	return m.Key + ": " + m.Values.String()
}

type object struct {
	Mappings []objectMapping
	KeyMap   map[string]int
}

func (o object) String() string {
	s := "{"
	for i, m := range o.Mappings {
		s += m.String()
		if i != len(o.Mappings)-1 {
			s += ", "
		}
	}
	return s + "}"
}

// Get returns the object's field.
func (o object) Get(key string) ValueSlice {
	idx, exists := o.KeyMap[key]
	if !exists {
		return nil
	}
	return o.Mappings[idx].Values
}

// GetFirst retrieves the first value of key's values, if any. If the key is not
// present or there are no values, nil is returned.
func (o object) GetFirst(key string) *value {
	idx, exists := o.KeyMap[key]
	if !exists {
		return nil
	}

	vals := o.Mappings[idx].Values
	if len(vals) == 0 {
		return nil
	}

	return &vals[0]
}

// Set sets a key-value property in the object. If the key already exists in the
// object, its value will be replaced with om.Values.
// It is expected of o.KeyMap to have been created through make()
func (o *object) Set(om objectMapping) {
	// If we already have the mapping, then we simply replace the previous
	// with the newer.
	if idx, exists := o.KeyMap[om.Key]; exists {
		o.Mappings[idx] = om
		return
	}
	// Otherwise, we create it.
	o.Mappings = append(o.Mappings, om)
	o.KeyMap[om.Key] = len(o.Mappings) - 1
}

var errExpectedDelimiter = errors.New("ld: expecting delimiter")

// ParseValues takes a json.Decoder and creates a ValueSlice, which is a
// representation of JSON data that basically discards arrays and mostly resides
// on the stack. ParseValues is mostly an internal function used by the ld
// package, and if you're an user of this package you should only ever need to
// use this when you're making a custom DocumentLoader.
func ParseValues(d *json.Decoder) (ValueSlice, error) {
	var (
		// level of depth we went through (how many unmatched brackets we have
		// left).
		arrayDepth uint
		vals       = make(ValueSlice, 0, 1)
	)
	for {
		// parse token
		t, err := d.Token()
		if err != nil {
			return nil, err
		}

		switch t := t.(type) {
		case json.Delim:
			// delimiter: for arrays, we simply keep track of the depth
			// (JSON-LD doesn't allow for nested arrays)
			// for objects, we hand them over to parse object
			switch t {
			case '[':
				arrayDepth++
			case ']':
				arrayDepth--
			case '{':
				obj, err := parseObject(d)
				if err != nil {
					return nil, err
				}
				vals = append(vals, value{
					Kind:   kindObject,
					Object: obj,
				})
			}
		case string:
			vals = append(vals, value{
				Kind:   kindString,
				String: t,
			})
		case json.Number:
			// json.Number is actually just a string, so we can handle it
			// as such.
			vals = append(vals, value{
				Kind:   kindNumber,
				String: string(t),
			})
		case bool:
			if t {
				vals = append(vals, value{Kind: kindTrue})
			} else {
				vals = append(vals, value{Kind: kindFalse})
			}
		case nil:
			vals = append(vals, value{Kind: kindNull})
		}

		// when arrayDepth reaches 0, either we never started an array in the
		// first place and this was just a single value, or the top-level array
		// we were in got closed.
		if arrayDepth == 0 {
			return vals, nil
		}
	}
}

var errDuplicateProperty = errors.New("ld: duplicate property in JSON object")

func parseObject(d *json.Decoder) (o object, err error) {
	// create map - to avoid assignments to nil maps
	o.KeyMap = make(map[string]int)

	var t json.Token
	for {
		t, err = d.Token()
		if err != nil {
			return
		}

		// delim automatically means closing bracket, so we need to return.
		if _, ok := t.(json.Delim); ok {
			return
		}

		// since this is an object, we know already the first token will be a
		// string
		key := t.(string)

		// parse value(s)
		var vals ValueSlice
		vals, err = ParseValues(d)
		if err != nil {
			return
		}

		// append the new key-value mapping
		(&o).Set(objectMapping{
			Key:      key,
			Values:   vals,
			IsSingle: len(vals) == 1,
		})
	}
}

// encodeBinary provides a fast binary encoding of vs - which should mostly be
// used for hashing the ValueSlice.
func (vs ValueSlice) encodeBinary() []byte {
	// We give an initial cap of 8*len(vs) - this is on the assumption that most
	// values will be objects or string/numbers 7 bytes long. Of course, this is
	// not always the case, and more often than not there are more than 6 bytes,
	// especially when one of the values is an object, but in those cases, Go's
	// automatic slice growth will kick in. The additional byte is for the first
	// byte - aka vs's length.
	b := make([]byte, 0, 8*len(vs)+1)
	// TODO: If we want encodeBinary to actually work for binary encoding vs, we
	// should encode every length (here and later on) as a varint.
	b = append(b, byte(len(vs)))

	for _, v := range vs {
		b = append(b, byte(v.Kind))
		switch v.Kind {
		case kindString, kindNumber:
			// strings/numbers: encode length, and write string directly
			b = append(b, byte(len(v.String)))
			// legal: https://golang.org/pkg/builtin/#append
			b = append(b, v.String...)
		case kindObject:
			// object: encode amount of keys, and for each key-value pair,
			// write the
			b = append(b, byte(len(v.Object.Mappings)))
			for _, m := range v.Object.Mappings {
				if m.IsSingle {
					b = append(b, 1)
				} else {
					b = append(b, 0)
				}
				b = append(b, byte(len(m.Key)))
				b = append(b, m.Key...)
				b = append(b, m.Values.encodeBinary()...)
			}
		}
	}

	return b
}
