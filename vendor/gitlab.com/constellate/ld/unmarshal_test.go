package ld

import (
	"io"
	"reflect"
	"strings"
	"testing"
	"time"
)

var commonLoader = &defaultDocumentLoader{
	cache: make(map[string]*cachedObject),
}

func testUnmarshal(v interface{}, str string) error {
	o, err := Parse(strings.NewReader(str))
	if err != nil {
		return err
	}
	o.Loader = commonLoader
	err = o.ParseContext()
	if err != nil {
		return err
	}
	err = o.Unmarshal(v, o)
	return err
}

type fataller interface {
	Fatalf(format string, values ...interface{})
}

func equal(f fataller, got, want interface{}) {
	if !reflect.DeepEqual(want, got) {
		if w, ok := want.([]byte); ok {
			want = string(w)
		}
		if g, ok := got.([]byte); ok {
			got = string(g)
		}
		f.Fatalf("want %v got %v", want, got)
	}
}

const simpleObject = `{
	"@context": {
		"whee": {
			"@type": "@id",
			"@id": "https://w3.org/whee"
		}
	},
	"whee": "sup"
}`

const moonFlight = `{
	"@context": {
		"to": {
			"@id": "https://flights.org/to",
			"@type": "@id"
		},
		"who": {
			"@id": "https://flights.org/person",
			"@type": "@id"
		}
	},
	"@type": "flight",
	"who": "me",
	"to": "https://flights.org/destinations/TheMoon"
}`

// and let me play among the stars...

const nestedObject = `{
	"_:t1": ` + moonFlight + `
}`

const mastodonProfile = `{
	"@context": [
		"https://www.w3.org/ns/activitystreams",
		"https://w3id.org/security/v1",
		{
			"manuallyApprovesFollowers": "as:manuallyApprovesFollowers",
			"sensitive": "as:sensitive",
			"Hashtag": "as:Hashtag",
			"ostatus": "http://ostatus.org#",
			"atomUri": "ostatus:atomUri",
			"inReplyToAtomUri": "ostatus:inReplyToAtomUri",
			"conversation": "ostatus:conversation",
			"toot": "http://joinmastodon.org/ns#",
			"Emoji": "toot:Emoji"
		}
	],
	"id": "https://mastodon.social/users/Gargron",
	"type": "Person",
	"following": "https://mastodon.social/users/Gargron/following",
	"followers": "https://mastodon.social/users/Gargron/followers",
	"inbox": "https://mastodon.social/users/Gargron/inbox",
	"outbox": "https://mastodon.social/users/Gargron/outbox",
	"preferredUsername": "Gargron",
	"name": "Eugen",
	"summary": "desc",
	"url": "https://mastodon.social/@Gargron",
	"manuallyApprovesFollowers": false,
	"publicKey": {
		"id": "https://mastodon.social/users/Gargron#main-key",
		"owner": "https://mastodon.social/users/Gargron",
		"publicKeyPem": "public key"
	},
	"endpoints": {
		"sharedInbox": "https://mastodon.social/inbox"
	},
	"icon": {
		"type": "Image",
		"mediaType": "image/png",
		"url": "https://files.mastodon.social/accounts/avatars/000/000/001/original/8e3dd118a025f184.png"
	}
}`

func TestUnmarshal(t *testing.T) {
	t.Parallel()
	t.Run("MastodonProfile", func(t *testing.T) {
		t.Parallel()
		const ld = mastodonProfile
		var s struct {
			ID                string `ld:"id"`
			Type              string
			PreferredUsername string
		}

		err := testUnmarshal(&s, ld)
		equal(t, err, nil)
		equal(t, s.ID, "https://mastodon.social/users/Gargron")
		equal(t, s.Type, "Person")
		equal(t, s.PreferredUsername, "Gargron")
	})
	t.Run("Simple", func(t *testing.T) {
		const ld = simpleObject
		var s struct {
			Whee string
		}

		err := testUnmarshal(&s, ld)
		equal(t, err, nil)
		equal(t, s.Whee, "sup")
	})
	t.Run("SimpleWithAbsoluteIRI", func(t *testing.T) {
		const ld = simpleObject
		var s struct {
			Whee string `ld:"https://w3.org/whee"`
		}

		err := testUnmarshal(&s, ld)
		equal(t, err, nil)
		equal(t, s.Whee, "sup")
	})
	t.Run("SimpleWithEmbedded", func(t *testing.T) {
		const ld = simpleObject
		type S1 struct {
			Whee string
		}
		var s struct {
			S1
		}

		err := testUnmarshal(&s, ld)
		equal(t, err, nil)
		equal(t, s.Whee, "sup")
	})
	t.Run("SimpleWithEmbeddedUnexported", func(t *testing.T) {
		const ld = simpleObject
		type s1 struct {
			Whee string
		}
		var s struct {
			s1
		}

		err := testUnmarshal(&s, ld)
		equal(t, err, nil)
		// Unmarshal should not have touched Whee
		equal(t, s.Whee, "")
	})
	t.Run("NestedMoonFlight", func(t *testing.T) {
		const ld = nestedObject
		var s struct {
			MoonFlight1 struct {
				Who string `ld:"https://flights.org/person"`
			} `ld:"_:t1"`
		}

		err := testUnmarshal(&s, ld)
		equal(t, err, nil)
		equal(t, s.MoonFlight1.Who, "me")
	})
	t.Run("Time", func(t *testing.T) {
		const ld = `{"_:time": "2009-10-22T00:00:01Z"}`
		var s struct {
			Time time.Time `ld:"_:time"`
		}

		err := testUnmarshal(&s, ld)
		equal(t, err, nil)
		equal(t, s.Time, time.Date(2009, 10, 22, 0, 0, 1, 0, time.UTC))
	})
}

func BenchmarkFullUnmarshal(b *testing.B) {
	const ld = mastodonProfile
	var s struct {
		Whee string
	}

	reader := strings.NewReader(ld)

	run := func() {
		o, err := Parse(reader)
		o.Loader = commonLoader
		equal(b, err, nil)
		err = o.ParseContext()
		equal(b, err, nil)
		err = o.Unmarshal(&s, o)
		equal(b, err, nil)
		reader.Seek(0, io.SeekStart)
	}

	run()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		run()
	}
}

func BenchmarkFullUnmarshalWithCache(b *testing.B) {
	const ld = mastodonProfile
	var s struct {
		Whee string
	}

	reader := strings.NewReader(ld)
	cacher := new(RolloverCache)

	run := func() {
		o, err := Parse(reader)
		o.Loader = commonLoader
		equal(b, err, nil)
		o.Cacher = cacher
		err = o.ParseContext()
		equal(b, err, nil)
		err = o.Unmarshal(&s, o)
		equal(b, err, nil)
		reader.Seek(0, io.SeekStart)
	}

	// warm it up
	run()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		run()
	}
}
