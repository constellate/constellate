# ld [![GoDoc](https://godoc.org/gitlab.com/constellate/ld?status.svg)](https://godoc.org/gitlab.com/constellate/ld)

## General outline

Last updated: 2017-11-26 @ fe68394aeb57e8797c4ac6b68f2f870b929895d8

* `parse.go` contains the code related to the JSON parsing in ld's custom data
  structure. It allows for fast parsing before unmarshalling. Everything is
  organised around three types: `value`, `ValueSlice` and `object`. ValueSlice
  represents a possible JSON array, although it makes no real distinction
  between arrays and single values: in fact, when ParseValues encounters a
  string at the top level, it will simply create a ValueSlice, holding one value
  of kind string. Nested arrays are also flattened as a consequence.
  * `value` holds the Kind of the value, which may be null, true/false, number,
    string or an object. Number and strings are both treated as strings - and
    for them, the `String` field is set. Instead, for objects, the `Object`
    field is filled out, and it will contain the key-value pairings of the
    object.
* Parse basically runs the JSON input through ParseValues, and retrives the
  first `object`, then wraps it into a nice exported `Object`, which provides
  functions for marshalling/unmarshalling.
* `context.go` contains pretty much all of the code of the
  [Context Processing Algorithms.](https://www.w3.org/TR/json-ld-api/#context-processing-algorithms)
