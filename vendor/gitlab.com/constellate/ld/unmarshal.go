package ld

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"
	"unicode"
)

var (
	errNotPointerToStruct = errors.New("ld: v is not pointer to struct")
	errVIsNil             = errors.New("ld: v is nil")
)

// Unmarshal places the object's data into v, which must be a pointer to a
// struct.
//
// First of all, the object's context is parsed, using Object.DocumentLoader to
// fetch remote contexts. This allows us to expand all the object's property
// names. For instance, the following object's property names:
//
//   {
//     "@context": "http://json-ld.org/contexts/person.jsonld",
//     "name": "Manu Sporny",
//     "homepage": "http://manu.sporny.org/",
//     "image": "http://manu.sporny.org/images/manu.png"
//   }
//
// Are expanded to this:
//
//   {
//     "http://schema.org/name": "Manu Sporny",
//     "http://schema.org/url": "http://manu.sporny.org/",
//     "http://schema.org/image": "http://manu.sporny.org/images/manu.png"
//   }
//
// Note the values are untouched: this is because we don't need to process the
// values just yet: they are processed on-demand, only if we need to place the
// value into the struct.
//
// Done this, the struct's fields are then iterated through. If the field has a
// tag, in the form of `ld:"propertyName"`, then its value is expanded following
// structContext. If there is no field tag, then the field name, with the first
// letter lowercased, is used by default. This is because generally in JSON-LD
// type names AreCapitalized, whereas property names areNot.
//
// Once the field's name is expanded, it is matched with the object's
// corresponding property, if any.
func (o *Object) Unmarshal(v interface{}, structContext *Object) error {
	if v == nil {
		return errVIsNil
	}

	// fast path: v is an Object itself
	if vObj, ok := v.(*Object); ok {
		*vObj = *o
		return nil
	}

	val := reflect.ValueOf(v)
	if val.Kind() != reflect.Ptr {
		return errNotPointerToStruct
	}
	val = val.Elem()
	if val.Kind() != reflect.Struct {
		return errVIsNil
	}
	vType := val.Type()

	// keyMap contains all the expanded property name to value mappings.
	keyMap := o.expandKeys()

	// proc is a processor holding the struct context. It is used to expand the
	// struct field names.
	proc := &processor{
		ActiveContext: structContext.parsedCtx,
	}

	var err error

	numFields := vType.NumField()
	for i := 0; i < numFields; i++ {
		fType := vType.Field(i)

		if fType.Anonymous {
			// check that this is a struct and the field we're looking at is
			// exported.
			if fType.Type.Kind() != reflect.Struct ||
				!unicode.IsUpper(rune(fType.Name[0])) {
				continue
			}
			o.Unmarshal(val.Field(i).Addr().Interface(), structContext)
			continue
		}

		// determine the desired name of the term.
		term, _ := termName(fType)

		// expand term, so that we can match it.
		exp, _ := proc.expandIRI(term, true, false)
		if _, isKeyword := keywordMap[exp]; !strings.Contains(exp, ":") && !isKeyword {
			continue
		}

		// get the matching value in the JSON-LD object, if any: continue otherwise
		matching := keyMap[exp]
		if len(matching) == 0 {
			continue
		}

		// When the field is a slice, we go through all the matching values:
		// the case where the matching value is an array itself. Otherwise we
		// just pick the first item.
		// As a special case, []byte (or []uint8) are handled as a single value.
		if fType.Type.Kind() == reflect.Slice &&
			fType.Type.Elem().Kind() != reflect.Uint8 {
			sl := reflect.MakeSlice(fType.Type.Elem(), len(matching), len(matching))
			for idx, v := range matching {
				err = proc.convertValue(sl.Index(idx), v, exp)
				if err != nil {
					return err
				}
			}
			val.Field(i).Set(sl)
		} else {
			err = proc.convertValue(val.Field(i), matching[0], exp)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func termName(fType reflect.StructField) (val, opts string) {
	val = fType.Tag.Get("ld")

	if idx := strings.Index(val, ","); idx != -1 {
		val, opts = val[:idx], val[idx+1:]
	}
	if val == "" {
		val = string(unicode.ToLower(rune(fType.Name[0]))) + fType.Name[1:]
	}
	return
}

// expandKeys iterates over the keys of the Object, and expands their property
// names.
func (o *Object) expandKeys() map[string][]value {
	// create map in which we will add all of the expanded keys and their
	// matching values
	m := make(map[string][]value, len(o.o.Mappings))

	// keys are expanded following the first part of the expansion algorithm:
	// https://www.w3.org/TR/json-ld-api/#expansion-algorithm
	proc := o.getProcessor()
	for _, mapping := range o.o.Mappings {
		if mapping.Key == "@context" {
			continue
		}

		iri, _ := proc.expandIRI(mapping.Key, true, false)

		_, isKeyword := keywordMap[iri]
		if !strings.Contains(iri, ":") && !isKeyword {
			continue
		}

		// We are doing append because there might be two properties which map
		// to the same value. This is the case of an object that has both a
		// "content" and a "contentMap" (in ActivityStreams).
		m[iri] = append(m[iri], mapping.Values...)
	}

	return m
}

// convertValue puts into dst the underlying value of src.
func (p *processor) convertValue(dst reflect.Value, src value, expandedTerm string) error {
	// fast path: src is null. We leave dst intact (which is in most cases the
	// zero value)
	if src.Kind == kindNull {
		return nil
	}

	// dst is an *Object: if src is an object as well, or a string having type
	// @id in the context, then we convert it to an *Object.
	if _, ok := dst.Interface().(*Object); ok {
		p.setObject(dst, src, expandedTerm)
		return nil
	}

	// dereference pointers or create new value if nil
	if dst.Kind() == reflect.Ptr {
		// check if dst is nil - in that case, before reassigning dst to be its
		// underlying element, we need to create the actual underlying value
		// we're going to modify.
		if dst.IsNil() {
			dst.Set(reflect.New(dst.Type().Elem()))
		}

		dst = dst.Elem()
	}

	// We handle time.Time specially - seeing as trying to read it as a struct
	// is pretty much pointless.
	if _, ok := dst.Interface().(time.Time); ok {
		t, _ := time.Parse(time.RFC3339, stringValue(src))
		dst.Set(reflect.ValueOf(t))
		return nil
	}

	kind := dst.Kind()
	t := dst.Type()

	// simple type conversions
	switch {
	case kind == reflect.String:
		dst.Set(reflect.ValueOf(stringValue(src)))
	case kind == reflect.Slice && dst.Elem().Kind() == reflect.Uint8:
		dst.Set(reflect.ValueOf([]byte(stringValue(src))))

	// number conversion
	case kind >= reflect.Int && kind <= reflect.Int64:
		i, _ := numberValue(src).Int64()
		dst.SetInt(i)
	case kind >= reflect.Uint && kind <= reflect.Uint64:
		i, _ := numberValue(src).Int64()
		dst.SetUint(uint64(i))
	case kind == reflect.Float64, kind == reflect.Float32:
		i, _ := numberValue(src).Float64()
		dst.SetFloat(i)

	// boolean
	case kind == reflect.Bool:
		dst.SetBool(src.Kind == kindTrue)

	// map[string]string
	case kind == reflect.Map &&
		t.Key().Kind() == reflect.String && t.Elem().Kind() == reflect.String:
		dst.Set(reflect.ValueOf(src.Object.buildMap()))

	// at this point, we've given up on the type, and we will only procede by
	// recursively calling Unmarshal if this is a struct.
	case kind != reflect.Struct:
		return fmt.Errorf("ld: unhandleable field type: %s", kind.String())
	}

	// src is not an object - nothing we can do
	if src.Kind != kindObject {
		return nil
	}

	childObject := (&Object{
		Loader:         p.Loader,
		o:              src.Object,
		parsedCtx:      p.ActiveContext,
		needsClone:     true,
		activeProperty: expandedTerm,
	})
	err := childObject.ParseContext()
	if err != nil {
		return err
	}
	return childObject.Unmarshal(dst.Addr().Interface(), childObject)
}

func (p *processor) setObject(dst reflect.Value, src value, expandedTerm string) {
	var obj object

	switch src.Kind {
	case kindString:
		if p.ActiveContext.termDefinitions[expandedTerm].Type != "@id" {
			dst.Set(reflect.Value{})
			return
		}

		// Create object holding a string, that being
		(&obj).Set(objectMapping{"@id", []value{{
			Kind:   kindString,
			String: src.String,
		}}, true})
	case kindObject:
		obj = src.Object
	default:
		dst.Set(reflect.Value{})
		return
	}

	dst.Set(reflect.ValueOf(&Object{
		Loader:         p.Loader,
		o:              obj,
		parsedCtx:      p.ActiveContext,
		needsClone:     true,
		activeProperty: expandedTerm,
	}))
}

func stringValue(v value) string {
	switch v.Kind {
	case kindString:
		return v.String
	case kindObject:
		// carry on
	default:
		return ""
	}

	if v := v.Object.GetFirst("@value"); v != nil {
		return v.String
	}
	if i := v.Object.GetFirst("@id"); i != nil {
		return i.String
	}
	return ""
}

func numberValue(v value) json.Number {
	switch v.Kind {
	case kindNumber, kindString:
		return json.Number(v.String)
	case kindNull, kindFalse:
		return "0"
	case kindTrue:
		return "1"
	}
	val := v.Object.GetFirst("@value")
	if val == nil {
		return ""
	}
	return json.Number(val.String)
}

func (o object) buildMap() map[string]string {
	m := make(map[string]string, len(o.Mappings))
	for _, mapping := range o.Mappings {
		if len(mapping.Values) == 0 {
			continue
		}
		if mapping.Values[0].Kind != kindString {
			continue
		}
		m[mapping.Key] = mapping.Values[0].String
	}
	return m
}
