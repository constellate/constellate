package ld

import (
	"bytes"
	"encoding/json"
	"errors"
	"reflect"
	"strconv"
	"time"
	"unicode"
)

var errNotAStruct = errors.New("ld: not a struct")

// Marshal converts v into JSON-LD data.
func (o *Object) Marshal(v interface{}) ([]byte, error) {
	val := reflect.ValueOf(v)
	if val.Kind() == reflect.Ptr {
		if val.IsNil() {
			return nil, errVIsNil
		}
		val = val.Elem()
	}
	if val.Kind() != reflect.Struct {
		return nil, errNotAStruct
	}

	obj := o.objectFromStruct(val, nil)
	m := obj.toMarshalerMap()
	switch len(o.originalCtx) {
	case 0: // ignore
	case 1:
		m["@context"] = o.originalCtx[0]
	default:
		m["@context"] = o.originalCtx
	}

	return json.Marshal(m)
}

// objectFromStruct generates an object from a struct.
func (o *Object) objectFromStruct(v reflect.Value, obj *object) object {
	vType := v.Type()
	num := v.NumField()

	if obj == nil {
		obj = &object{
			Mappings: make([]objectMapping, 0, num+1),
			KeyMap:   make(map[string]int, num+1),
		}
		// obj == nil, meaning this is not an embedded struct.
		// we should then set the @type.
		// If you want to set a custom type, create a field and set its `ld` tag
		// to @type.
		if vName := vType.Name(); vName != "" {
			inv := o.inverseIRI("@type")
			obj.Set(objectMapping{inv, ValueSlice{{Kind: kindString, String: vName}}, true})
		}
	}

	for i := 0; i < num; i++ {
		f := v.Field(i)
		fType := vType.Field(i)

		if fType.Anonymous {
			if !unicode.IsUpper(rune(fType.Name[0])) || f.Kind() != reflect.Struct {
				continue
			}
			o.objectFromStruct(v.Field(i), obj)
			continue
		}

		name, opts := termName(fType)

		// determine whether the omitempty option is enabled
		omitEmpty := opts == "omitempty"

		if f.Kind() != reflect.Slice {
			res, ok := o.valueFromGoValue(f)
			// check that we have a value and that we do not proceed when
			// omitEmpty and res.Empty() are both true.
			if ok && !(omitEmpty && res.Empty()) {
				obj.Set(objectMapping{name, ValueSlice{res}, true})
			}
			continue
		}

		l := f.Len()
		vs := make(ValueSlice, 0, l)
		for i := 0; i < l; i++ {
			res, ok := o.valueFromGoValue(f.Index(i))
			if ok && !(omitEmpty && res.Empty()) {
				vs = append(vs, res)
			}
		}
		forceArray := o.parsedCtx.termDefinitions[name].ContainerMapping != ""
		if !(omitEmpty && len(vs) == 0) {
			obj.Set(objectMapping{name, vs, len(vs) == 1 && !forceArray})
		}
	}
	return *obj
}

func (o *Object) valueFromGoValue(v reflect.Value) (value, bool) {
	k := v.Kind()

	// if it's a pointer, check if it's nil. If it is, then
	// return null as a json value. otherwise, get its element.
	if k == reflect.Ptr {
		if v.IsNil() {
			return value{Kind: kindNull}, true
		}
		v = v.Elem()
		k = v.Kind()
	}

	switch v := v.Interface().(type) {
	case Object:
		return value{Kind: kindObject, Object: v.o}, true
	case time.Time:
		return value{Kind: kindString, String: v.Format(time.RFC3339)}, true
	}

	switch k {
	case reflect.Bool:
		// bool: set kind to kindTrue or kindFalse accordingly
		if v.Bool() {
			return value{Kind: kindTrue}, true
		}
		return value{Kind: kindFalse}, true

	case reflect.String:
		// string: set kindString and place the string
		return value{Kind: kindString, String: v.String()}, true

	case reflect.Struct:
		// struct: create object
		obj := o.objectFromStruct(v, nil)
		return value{Kind: kindObject, Object: obj}, true

	case reflect.Map:
		// map: we can only handle map[string]string.
		m, ok := v.Interface().(map[string]string)
		if !ok {
			return value{}, false
		}
		obj := &object{
			Mappings: make([]objectMapping, 0, len(m)),
			KeyMap:   make(map[string]int, len(m)),
		}
		for k, v := range m {
			obj.Set(objectMapping{k, ValueSlice{{Kind: kindString, String: v}}, true})
		}
		return value{Kind: kindObject, Object: *obj}, true

	case reflect.Float32, reflect.Float64:
		// float: parse and place in value
		val := value{Kind: kindNumber}
		if k == reflect.Float32 {
			val.String = strconv.FormatFloat(v.Float(), 'f', -1, 32)
		} else {
			val.String = strconv.FormatFloat(v.Float(), 'f', -1, 64)
		}
		return val, true
	}

	// other ints: parse and place in value
	switch {
	case k >= reflect.Int && k <= reflect.Int64:
		return value{Kind: kindNumber, String: strconv.FormatInt(v.Int(), 10)}, true
	case k >= reflect.Uint && k <= reflect.Uint64:
		return value{Kind: kindNumber, String: strconv.FormatUint(v.Uint(), 10)}, true
	}

	// other kind: nothing we can do.
	return value{}, false
}

func (o object) toMarshalerMap() map[string]json.Marshaler {
	m := make(map[string]json.Marshaler, len(o.Mappings))
	for _, mapping := range o.Mappings {
		if mapping.IsSingle {
			m[mapping.Key] = mapping.Values[0]
		} else {
			m[mapping.Key] = mapping.Values
		}
	}
	return m
}

func (o object) MarshalJSON() ([]byte, error) {
	return json.Marshal(o.toMarshalerMap())
}

var (
	byteNull         = []byte("null")
	byteTrue         = []byte("true")
	byteFalse        = []byte("false")
	errUndefinedKind = errors.New("ld: undefined kind")
)

func (v value) MarshalJSON() ([]byte, error) {
	switch v.Kind {
	case kindNull:
		return byteNull, nil
	case kindTrue:
		return byteTrue, nil
	case kindFalse:
		return byteFalse, nil
	case kindObject:
		return v.Object.MarshalJSON()
	case kindString:
		return json.Marshal(v.String)
	case kindNumber:
		return []byte(v.String), nil
	}
	return nil, errUndefinedKind
}

// MarshalJSON implements json marshalling for ValueSlice.
func (v ValueSlice) MarshalJSON() ([]byte, error) {
	b := bytes.NewBuffer(make([]byte, 0, len(v)*15))
	b.WriteByte('[')
	for idx, val := range v {
		bNew, err := val.MarshalJSON()
		if err != nil {
			return nil, err
		}
		b.Write(bNew)
		if idx != len(v)-1 {
			b.WriteByte(',')
		}
	}
	b.WriteByte(']')
	return b.Bytes(), nil
}
