/*
Package ld provides functions to parse, unmarshal and marshal JSON-LD data
structures, in an efficient manner and bringing static typing. It does not aim
to be a JSON-LD 100% compliant processor; in fact, at the moment it's not even
capable of any of the compaction, expansion or flattening algorithm, however it
does correctly parse contexts and expand IRIs, so that they can be mapped to
structs.

JSON parsing

Mostly for efficiency, ld does not use off-the-shelf JSON unmarshalling to do
the initial decoding of data - instead, it parses the raw JSON tokens and places
them in an ad-hoc data structure. One of its features is that it does not really
make any difference between elements contained inside an array and elements that
are not - for this reason it is sometimes more lenient than other JSON parsers.
This should not bring any issues (on the contrary, the carelessness of arrays
is a feature caused by JSON-LD's approach to arrays).
*/
package ld

import (
	"encoding/json"
	"fmt"
	"io"

	"github.com/cespare/xxhash"
)

// Parse decodes JSON-LD data from r.
func Parse(r io.Reader) (*Object, error) {
	d := json.NewDecoder(r)
	d.UseNumber()
	vals, err := ParseValues(d)
	if err != nil {
		return nil, err
	}
	for _, val := range vals {
		if val.Kind == kindObject {
			return &Object{
				o:         val.Object,
				parsedCtx: &context{},
			}, nil
		}
	}
	// no objects found: return empty object
	return &Object{
		parsedCtx: &context{},
	}, nil
}

// Object represents a JSON-LD object that needs yet to be unmarshalled into a
// Go struct.
type Object struct {
	Loader DocumentLoader
	Cacher ContextCacher

	o           object
	parsedCtx   *context
	originalCtx ValueSlice
	// determines whether parsedCtx needs to be cloned when parsing new context.
	// This is the case of sub-objects having a shared context with the parent.
	needsClone bool
	// current property being parsed - only set if this is a child element of
	// another object.
	activeProperty string
}

func (o *Object) getProcessor() *processor {
	return &processor{
		processorOptions: processorOptions{Loader: o.Loader},
		ActiveContext:    o.parsedCtx,
	}
}

// ParseContext creates a new context from the Object's @context JSON field.
func (o *Object) ParseContext() error {
	ctxProp := o.o.Get("@context")
	if len(ctxProp) == 0 {
		// nothing to do
		return nil
	}

	var hash uint64
	if o.Cacher != nil {
		hash = xxhash.Sum64(ctxProp.encodeBinary())
		toRet := o.Cacher.Get(hash)
		if toRet != nil {
			o.parsedCtx = toRet.parsedCtx
			return nil
		}
	}

	if o.needsClone {
		o.parsedCtx = o.parsedCtx.clone()
		o.needsClone = false
	}

	proc := o.getProcessor()
	proc.LocalContexts = ctxProp
	o.originalCtx = ctxProp
	err := proc.processContext(nil)
	if o.Cacher == nil || err != nil {
		return err
	}
	o.Cacher.Set(hash, &Object{parsedCtx: o.parsedCtx})
	return nil
}

// PeekType looks through the the attributes of the Object, to determine if any
// of them can be expanded to "@type". If one can, its value is returned.
func (o *Object) PeekType() string {
	// iterating over the keys in the object, see if any has a mapping to "@type"
	inv := o.inverseIRI("@type")
	if inv == "" {
		return ""
	}
	vals := o.o.Get(inv)

	// technically, vals can be a []string, but just for keeping things simple
	// we're gonna straight out ignore any @type value which has more than one
	// value.
	if len(vals) != 1 || vals[0].Kind != kindString {
		return ""
	}

	expanded, err := o.getProcessor().expandIRI(vals[0].String, true, true)
	if err != nil {
		// not supposed to happen: expandIRI never returns an error when defined
		// is nil.
		panic(fmt.Errorf("ld: unexpected error while expanding IRI: %v", err))
	}
	return expanded
}

func (o *Object) inverseIRI(s string) string {
	for _, mapping := range o.o.Mappings {
		if o.parsedCtx.termDefinitions[mapping.Key].IRIMapping == s {
			return mapping.Key
		}
	}
	return s
}
