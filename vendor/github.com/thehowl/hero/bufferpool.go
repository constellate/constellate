package hero

import (
	bbp "github.com/valyala/bytebufferpool"
)

// GetBuffer returns a ByteBuffer from the default pool.
func GetBuffer() *bbp.ByteBuffer {
	return bbp.Get()
}

// PutBuffer returns a ByteBuffer to the default pool..
func PutBuffer(buffer *bbp.ByteBuffer) {
	if buffer == nil {
		return
	}

	bbp.Put(buffer)
}
